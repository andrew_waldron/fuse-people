angular.module('fuse-people.controllers', [])

.controller('TeamsCtrl', function($scope, Teams) {
  // should be loaded from a resource
  $scope.teams = Teams.all();
})
.controller('MembersCtrl', function($scope, $stateParams, Teams) {
  $scope.selectedTeam = Teams.byId($stateParams.teamId);
})

.controller('PeopleCtrl', function($scope, $stateParams, People, $cordovaCamera) {
  $scope.noSelection = !$stateParams.personId;

  $scope.takeAPicture = function() {
    debugger;

    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      var image = document.getElementById('freshphoto');
      image.src = "data:image/jpeg;base64," + imageData;
    }, function(err) {
      // error
    });
  };

  if ($stateParams.personId) {
    $scope.selectedPerson = People.byId($stateParams.personId);
  }
});
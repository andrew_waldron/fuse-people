angular.module('fuse-people.services', [])
.factory('Teams', function() {
  var teams = [
    {id: 1, name: "Shenanigans", icon: "img/team-icons/shenanigans.jpg"},
    {id: 2, name: "Shark Bytes", icon: "img/team-icons/shark-bytes.jpg"},
    {id: 3, name: "Master Builders", icon: "img/team-icons/master-builders.png"}
  ];

  teams[0].members = [
      {id: 1, name: 'John Ryan'},
      {id: 2, name: 'Andrew Waldron'},
      {id: 3, name: 'Craig Anderson'},
      {id: 4, name: 'Ron Carpenter'},
      {id: 5, name: 'Doug Powell'},
      {id: 6, name: 'Rob Wheaton'},
      {id: 7, name: 'Brian Dunzweiler'},
      {id: 8, name: 'Mary Anderson'},
      {id: 9, name: 'Andrew Beard'},
      {id: 10, name: 'Matt Haberman'}
    ];

  teams[1].members = [
      {id: 11, name: 'Scott Cummins'},
      {id: 12, name: 'Keith Gasper'},
      {id: 13, name: 'Jen Picolo'},
      {id: 14, name: 'Mark Dickson'},
      {id: 15, name: 'Indira Akurathi'},
      {id: 16, name: 'Shaunn Barron'},
      {id: 17, name: 'Dan Fischer'},
      {id: 18, name: 'Calixto Melean'},
      {id: 19, name: 'Justin Hohman'},
      {id: 20, name: 'Scott Oster'},
      {id: 21, name: 'Kristen Furby'}
  ];

  teams[2].members = [{id: 1, name: 'Unknown Members'}];

  return {
    all: function () {
      return teams;
    },
    byId: function (id) {
      var selectedTeam;
      teams.forEach(function(team) {
        if (team.id == id) {
          selectedTeam = team;
        }
      });

      return selectedTeam;
    }
  };
})
.factory('People', function() {
  var people = [
      {id: 1, name: 'John Ryan', bio: 'Some details and bio information here'},
      {id: 2, name: 'Andrew Waldron', bio: 'Some details and bio information here about Andrew Waldron'},
      {id: 3, name: 'Craig Anderson', bio: 'Some details and bio information here about Craig Anderson'},
      {id: 4, name: 'Ron Carpenter', bio: 'Some details and bio information here about Ron Carpenter'},
      {id: 5, name: 'Doug Powell', bio: 'Some details and bio information here about Doug Powell'},
      {id: 6, name: 'Rob Wheaton', bio: 'Some details and bio information here about Rob Wheaton'},
      {id: 7, name: 'Brian Dunzweiler', bio: 'Some details and bio information here about Brian Dunzweiler'},
      {id: 8, name: 'Mary Anderson', bio: 'Some details and bio information here about Mary Anderson'},
      {id: 9, name: 'Andrew Beard', bio: 'Some details and bio information here about Andrew Beard'},
      {id: 10, name: 'Matt Haberman', bio: 'Some details and bio information here about Matt Haberman'}
  ];

  return {
    all: function () {
      return teams;
    },
    byId: function (id) {
      var selectedPerson;
      people.forEach(function(person) {
        if (person.id == id) {
          selectedPerson = person;
        }
      });

      return selectedPerson;
    }
  };
});

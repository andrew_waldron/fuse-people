angular.module('fuse-people', ['ionic', 'fuse-people.controllers', 'fuse-people.services', 'ngCordova'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  .state('tab.people', {
    url: '/people/:personId',
    views: {
      'tab-people': {
        templateUrl: 'templates/tab-people.html',
        controller: 'PeopleCtrl'
      }
    }
  })
  .state('tab.teams', {
      url: '/teams',
      views: {
        'tab-teams': {
          templateUrl: 'templates/tab-teams.html',
          controller: 'TeamsCtrl'
        }
      }
    })
  .state('tab.team-detail', {
    url: '/teams/:teamId',
    views: {
      'tab-teams': {
        templateUrl: 'templates/tab-members.html',
        controller: 'MembersCtrl'
      }
    }
  });

  $urlRouterProvider.otherwise('/tab/teams');
});
